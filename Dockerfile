FROM ruby:2.4.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /docker-rails-app
WORKDIR /docker-rails-app
ADD Gemfile /docker-rails-app/Gemfile
ADD Gemfile.lock /docker-rails-app/Gemfile.lock
RUN bundle install
ADD . /docker-rails-app