# config valid only for current version of Capistrano
lock "3.8.0"

set :application, "docker-rails-app"
set :repo_url, "https://bitbucket.org/frobert/docker-rails-app.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/hosting/docker-rails-app"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Define port range in respect to load balancer on server
# If 2 or more environments reside on same server, configure port range as per environment
# Ruby's Range object is expected, see http://ruby-doc.org/core-2.3.0/Range.html
# Example: set :docker_compose_port_range, 2070..2071
set :docker_compose_port_range, 2100..2900

# OPTIONAL
# User name when running the Docker image (reflecting Docker's USER instruction)
# Example: set :docker_compose_user, 'pioneer'
#set :docker_compose_user, '<username>'

# OPTIONAL
# Roles considered
# Defaults to :all
# Example: set :docker_compose_roles, :web
set :docker_compose_roles, %w{app db web}

namespace :setup do
  desc 'install docker'
  task :docker do
    on roles(:app) do
    	execute "apt-get update && apt-get upgrade -y"
    	execute 'curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose'
    	execute "chmod +x /usr/local/bin/docker-compose"
    	execute "apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D"
    	execute "apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main' && apt-get update"
    	execute "apt-get install -y docker-engine"
    end
  end
end